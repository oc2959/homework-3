"""
pend_GUI.py

Created By Omar Clinton
Last Modification Oct 21

Description:
____________

This class creates the layout of the GUI and interfaces with pend_mechanics. 
i.e, it sends the initial conditions to pend_mechanics to obtain the states 
of the links

"""

from numpy import arange, cos, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import Tkinter as Tk
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pend_mechanics as pm

class Application(Tk.Frame):
	def __init__(self, master=None):

		Tk.Frame.__init__(self, master)
		self.grid()

		self.master.title('Pendulum Simulation')
		self.master.geometry('{}x{}'.format(1000, 800))

		# subdivide into 21 rows and 6 columns

		for r in range(21):
			self.master.rowconfigure(r, weight=1)    
		for c in range(6):
				self.master.columnconfigure(c, weight=1)

		# run simulation and stop simulation buttons
		
		self.runsimBB = Tk.Button(master, text="Run Simulation",command=self.runSim, bg="green").grid(row=21,column=4,columnspan = 1,sticky=Tk.E+Tk.W)
		self.stopsimBB = Tk.Button(master, text="Stop Simulation",command=self.close_window, bg="red",foreground = "white").grid(row=21,column=5,columnspan = 1,sticky=Tk.E+Tk.W)

		#-----------------------------------------------------------
		# See GUILayout.png for more clarification of following text
		#-----------------------------------------------------------

		# grid_ = [[row,column,rowspan,columnspan]...]

		grid_ = [[0,0,1,4], [1,0,12,4], [13,0,1,3], [14,0,8,3], [13,3,1,1], [14,3,8,1],[0,4,20,2]]

		self.Frame = []

		for i in range(0,7):

			self.Frame.append(Tk.Frame(master,bg ="white"))
			self.Frame[i].grid(row = grid_[i][0], column = grid_[i][1], rowspan = grid_[i][2], columnspan = grid_[i][3], sticky = Tk.W+Tk.E+Tk.N+Tk.S)
		
		# Specify weights of columns

		self.Frame[6].grid_columnconfigure(0, weight=1)
		self.Frame[6].grid_columnconfigure(1, weight=1)

		# request that the window manager set the size to that exact dimension

		self.Frame[6].grid_propagate(False)
		self.Frame[6].grid_propagate(False)

		# Titles of the frames

		Frame_Titles = ['N-Tuple Pendulum Animation', 'Paramater Plot', 'Parameter To Plot' ]

		for i in range(0,3):

			var = Tk.StringVar()
			label = Tk.Label( self.Frame[2*i], textvariable=var, relief=Tk.RAISED,bg = "white" )
			var.set(Frame_Titles[i])
			label.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

		# Set scrollbar for parameter selection to plot listbox

		scrollbar = Tk.Scrollbar(self.Frame[5])
		scrollbar.pack( side = Tk.RIGHT, fill=Tk.Y )
		self.graphlist = Tk.Listbox(self.Frame[5], yscrollcommand = scrollbar.set)
		scrollbar.config(command=self.graphlist.yview)

		# Initialize listbox contents

		self.graphlist.insert(Tk.END, "theta1 (rad) vs time (s)")
		self.graphlist.insert(Tk.END, "thetadot1 (rad/s) vs time (s)")
		self.graphlist.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

		# select first element of list

		self.graphlist.selection_set(first=0) 

		# Callback for listbox seletion

		self.graphlist.bind('<<ListboxSelect>>', self.onselect)

		# array containing input labels,types and values

		labelArr = ['Time (s):', 'dt (s):', 'Gravity (m/s^2):', 'Length (m):', 'Mass (kg):' , 'Number Of Links:', 'Initial Conditions', 'Angle 1' , 'Angular Vel 1']
		type_ = ['entry', 'entry', 'entry', 'slider','slider', 'slider', ' ', 'slider','slider']
		value = ['5','0.01','9.81','0.1/10/0.1','0.1/10/0.1', '1/5/1','','-1.57/1.57/0.01','-5/5/0.01']

		# initialize widget arrays

		self.entry = []
		self.slider = []

		for i in range(0,9):

			var = Tk.StringVar()
			label = Tk.Label( self.Frame[6], textvariable=var, bg = "white" )
			var.set(labelArr[i])
			
			if (type_[i] == 'entry'):

				label.grid(row = i, column = 0,columnspan = 1,sticky = Tk.W)
				var = Tk.StringVar()
				self.entry.append(Tk.Entry(self.Frame[6],width=17,textvariable=var))
				var.set(float(value[i]))
				self.entry[len(self.entry)-1].grid(row =i, column = 1,columnspan = 1,sticky = Tk.E)

			elif (type_[i] == 'slider'):

				label.grid(row = i, column = 0,columnspan = 1,sticky = Tk.W)
				first = value[i].split('/')[0]
				last = value[i].split('/')[1]
				res = value[i].split('/')[2]

				self.slider.append(Tk.Scale(self.Frame[6], from_= first, to= last, resolution= res,orient=Tk.HORIZONTAL,bg = "white"))
				self.slider[len(self.slider)-1].grid(row =i, column = 1,columnspan = 1,sticky = Tk.E)

			else:

				label.configure(relief=Tk.RAISED)
				label.grid(row =i, column = 0,columnspan = 2,sticky = Tk.W+Tk.E+Tk.N+Tk.S)

		# add callback for third slider

		self.slider[2].configure(command = self.link_slider)

		# top animation and bottom animation figures

		self.fig1 = plt.Figure(figsize=(1,2), dpi=100)
		self.fig2 = plt.Figure(figsize=(1,1), dpi=100)

		# top and bottom canvases

		self.canvas1 = FigureCanvasTkAgg(self.fig1, self.Frame[1])
		self.canvas1.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

		self.canvas2 = FigureCanvasTkAgg(self.fig2, self.Frame[3])
		self.canvas2.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

		# initialize variables

		self.init_slider_arr = [] # array that holds the initial condition sliders
		self.init_label_arr = [] # array that holds the initial condition labels
		self.Narr = [] # array that keeps track of the number of links selected
		self.t = [] # array that holds the time
		self.sim_prog = [] # animation step

	def onselect(self,*args):

		if len(self.t) != 0:
			# ran simulation
			if len(self.t) != self.sim_prog + 1:
				# simulation did not finish
				# int(self.graphlist.curselection()[0] gives the index of the listbox selection
				# change axis size depending if it is an angle or a velocity plot
				if (int(self.graphlist.curselection()[0]) % 2 == 0):
					self.ax2.set_ylim(-3, 3)
					self.ax2.figure.canvas.draw()
				else:
					self.ax2.set_ylim(-50, 50)
					self.ax2.figure.canvas.draw()
			else:
				# simulation finished
				# show graphs by changing axes size to min(ydata) and max(ydata)
				xdata = self.t
				ydata = self.states[:,-1 + self.index[int(self.graphlist.curselection()[0])]]

				self.ax2.set_ylim(min(ydata), max(ydata))
				self.line2.set_data(xdata, ydata)
				self.ax2.figure.canvas.draw()

				
		
	def link_slider(self,*args):

		# get number of links
		N = self.slider[2].get()
		self.Narr.append(N)

		if len (self.Narr)>1:
			if (self.Narr[-1] - self.Narr[-2] < 0):

				# decreased number of links
				# hence delete corresponding labels and slider

				for x in range(0,2):

					# delete data from listbox
					# delete labels and sliders
						
					self.graphlist.delete(Tk.END) 
					self.init_slider_arr[-1].grid_remove()
					self.init_label_arr[-1].grid_remove()
					self.init_slider_arr.pop()
					self.init_label_arr.pop()
					
			elif(N >1):

				# increased number of links
				
				# add data to listbox
				# add labels and sliders

				self.graphlist.insert(Tk.END, "theta" + str(N) + "(rad) vs time (s)")
				self.graphlist.insert(Tk.END, "thetadot" + str(N) +  "(rad) vs time (s)")
				self.graphlist.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

				var = Tk.StringVar()
				self.init_label_arr.append(Tk.Label( self.Frame[6], textvariable=var, bg = "white" ))
				var.set("Angle " + str(N) + " (Radians):")
				self.init_label_arr[2*(N -2)].grid(row = 9+2*(N -2), column = 0,columnspan = 1,sticky = Tk.W)

				self.init_slider_arr.append(Tk.Scale(self.Frame[6], from_=-1.57, to=1.57, resolution = 0.01, orient=Tk.HORIZONTAL,bg = "white"))
				self.init_slider_arr[2*(N -2)].grid(row =9+2*(N -2), column = 1,columnspan = 1,sticky = Tk.E)

				var = Tk.StringVar()
				self.init_label_arr.append(Tk.Label( self.Frame[6], textvariable=var, bg = "white" ))
				var.set("Angular Velocity " + str(N) + " (Radians/Second):")
				self.init_label_arr[2*(N -2)+1].grid(row = 10+2*(N -2), column = 0,columnspan = 1,sticky = Tk.W)

				self.init_slider_arr.append(Tk.Scale(self.Frame[6], from_=-5, to=5, resolution = 0.01, orient=Tk.HORIZONTAL,bg = "white"))
				self.init_slider_arr[2*(N -2)+1].grid(row =10+2*(N -2), column = 1,columnspan = 1,sticky = Tk.E)

	def runSim(self):

		tf = float(self.entry[0].get()) # final time
		dt = float(self.entry[1].get()) # time step
		gg = float(self.entry[2].get()) # gravity
		link_l = self.slider[0].get() # length of link
		mass = self.slider[1].get() # mass of link
		N = self.slider[2].get() # number of links
		init_angle = [self.slider[3].get()] # initial angle of first link
		init_angle_vel = [self.slider[4].get()] # initial velocity of first link
		self.t = np.arange(0,tf,dt) # tie array

		for x in range(0,len(self.init_slider_arr)):
			if (x % 2) != 0:
				init_angle_vel.append(self.init_slider_arr[x].get())
			else:
				init_angle.append(self.init_slider_arr[x].get())

		# call simulateN from pend_mechanics class to obtain the states
			
		self.states = pm.pend_mechanics().simulateN(init_angle,init_angle_vel,N,link_l,mass,self.t,gg)

		# clear the figures

		self.fig1.clear()
		self.fig2.clear()

		# resize axis based on total link length

		ax1 = self.fig1.add_subplot(111, autoscale_on=False, xlim=(-N*link_l, N*link_l), ylim=(-N*link_l, N*link_l), aspect='equal')
		self.ax2 = self.fig2.add_subplot(111,xlim=(self.t[0], self.t[-1]))

		time_template = 'time = %.1fs'
		time_text = ax1.text(0.05, 0.9, '', transform=ax1.transAxes)

		numpoints = self.states.shape[1] / 2 + 1

		# initialization function: plot the background of each frame
		def init1():
			self.line1, = ax1.plot([],[],'o-', lw=2)
			return self.line1,

		def init2():
			self.line2, = self.ax2.plot([],[])
			return self.line2,

		# animation function.  This is called sequentially
		def animate1(i):
			x = np.zeros((numpoints))
			y = np.zeros((numpoints))
			for j in arange(1, numpoints):
				x[j] = x[j - 1] + link_l * sin(self.states[i, j-1])
				y[j] = y[j - 1] - link_l * cos(self.states[i, j-1])
			self.line1.set_data(x, y)
			time_text.set_text(time_template%(i*dt))
			return self.line1, time_text

		foo1 = np.arange(1,N+1,1)
		foo2 = np.arange(N+1,2*N+1)
		self.index = np.add(sum([[i,0] for i in foo1], []),sum([[0,i] for i in foo2], []))

		# self.graphlist.curselection()[0] corresponds to the selection in the listbox

		if len(self.graphlist.curselection()) > 0:
			if (int(self.graphlist.curselection()[0]) % 2 == 0):
				self.ax2.set_ylim(-3, 3)
				self.ax2.figure.canvas.draw()
			else:
				self.ax2.set_ylim(-50, 50)
				self.ax2.figure.canvas.draw()

		xdata = []
		ydata = []

		def animate2(i):

			self.sim_prog = i # simulation progress tracker

			if len(self.graphlist.curselection()) == 0:
				#nothing selcted in the listbox
				self.graphlist.selection_set(first=0)
				self.ax2.set_ylim(-3, 3)
				self.ax2.figure.canvas.draw()
			
			xdata = self.t[0:i]
			ydata = self.states[0:i,-1 + self.index[int(self.graphlist.curselection()[0])]]

			ymin, ymax = self.ax2.get_ylim()

			if i>0:
				if  max(ydata)>= ymax or min(ydata)<= ymin:
					# resize axes
					self.ax2.set_ylim(min(ydata), max(ydata))
					self.ax2.figure.canvas.draw()

			self.line2.set_data(xdata, ydata)

			return self.line2,

		# animations

		ani1 = animation.FuncAnimation(self.fig1, animate1, frames=len(self.t), init_func=init1,
                interval=self.t[-1] , blit=False, repeat=False)
		ani2 = animation.FuncAnimation(self.fig2, animate2, frames=len(self.t), init_func=init2, 
				interval = self.t[-1], blit=False,repeat=False)

		# show canvases

		self.canvas1.show()
		self.canvas2.show()

		Tk.mainloop()

	def close_window(self):
		self.master.destroy()

root = Tk.Tk()
app = Application(master=root)
root.mainloop()
