"""
pend_mechanics.py

Created By Omar Clinton
Last Modification Oct 21

Code from: http://www.moorepants.info/blog/npendulum.html

Description:
____________

This class computes and evaluates the equations 
to determine the angles and angular velocities 
of the links

"""

from sympy import symbols             # Import the symbols function
import sympy.physics.mechanics as spm # Import mechanics classes and functions
import pylab as py
from sympy import Dummy, lambdify
from scipy.integrate import odeint
from numpy import zeros, cos, sin, arange, array, pi
from matplotlib import pyplot as plt
from matplotlib import animation

class pend_mechanics(object):

    def simulateN(self,init_angle,init_angle_vel,N,_length,_mass,tt,gg):

        #N = 1 # Number of links in N-pendulum
        q = spm.dynamicsymbols('q:' + str(N))     # Generalized coordinates
        u = spm.dynamicsymbols('u:' + str(N))     # Generalized speeds
        m = symbols('m:' + str(N))            # Mass of each link
        l = symbols('l:' + str(N))            # Length of each link
        g, t = symbols('g t')                 # gravity and time symbols

        A = spm.ReferenceFrame('A')               # Inertial reference frame
        Frames = []                           # List to hold n link frames
        P = spm.Point('P')                        # Hinge point of top link
        P.set_vel(A, 0)                       # Set velocity of P in A to be zero
        Particles = []                        # List to hold N particles
        FL = []                               # List to hold N applied forces
        kd = []                               # List to hold kinematic ODE's

        for i in range(N):
            Ai = A.orientnew('A' + str(i), 'Axis', [q[i], A.z]) # Create a new frame
            Ai.set_ang_vel(A, u[i] * A.z)                       # Set angular velocity
            Frames.append(Ai)                                   # Add it to Frames list

            Pi = P.locatenew('P' + str(i), l[i] * Ai.x)  # Create a new point,
            Pi.v2pt_theory(P, A, Ai)                     # Set velocity
            Pai = spm.Particle('Pa' + str(i), Pi, m[i])      # Create a new particle
            Particles.append(Pai)                        # Add Pai to Particles list

            FL.append((Pi, m[i] * g * A.x)) # Set the force applied at i-th Point
            P = Pi                          # P now represents the lowest Point

            kd.append(q[i].diff(t) - u[i])  # Kinematic ODE:  dq_i / dt - u_i = 0

        KM = spm.KanesMethod(A, q_ind=q, u_ind=u, kd_eqs=kd) # Generate EoM's:
        fr, frstar = KM.kanes_equations(FL, Particles)   # fr + frstar = 0

        # NUMERICAL SIMULATION

        parameters = [g]                                             # Parameter Definitions
        parameter_vals = [gg]                                      # First we define gravity
        for i in range(N):
            parameters += [l[i], m[i]]                               # Then each mass
            parameter_vals += [_length, _mass]                     # and length

        dummy_symbols = [Dummy() for i in q + u]                     # Necessary to translate
        dummy_dict = dict(zip(q + u, dummy_symbols))                 # out of functions of time
        kds = KM.kindiffdict()                                       # Need to eliminate qdots
        MM = KM.mass_matrix_full.subs(kds).subs(dummy_dict)          # Substituting away qdots
        Fo = KM.forcing_full.subs(kds).subs(dummy_dict)              # and in dummy symbols
        mm = lambdify(dummy_symbols + parameters, MM)                # The actual call that gets
        fo = lambdify(dummy_symbols + parameters, Fo)                # us to a NumPy function

        def rhs(y, t, args):                                         # Creating the rhs function
            into = py.hstack((y, args))                                 # States and parameters
            return py.array(py.linalg.solve(mm(*into), fo(*into))).T[0]    # Solving for the udots

        y0 = py.hstack((init_angle, init_angle_vel))               # Initial conditions, q and u    
        y = odeint(rhs, y0, tt, args=(parameter_vals,))               # Actual integration

        return y
